class AddCategoryToGalery2 < ActiveRecord::Migration[7.0]
  def change
    remove_column :galeries, :category
  end
end
