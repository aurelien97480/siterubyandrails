class AddCategoryToGalery < ActiveRecord::Migration[7.0]
  def change
    add_reference :galeries, :category, foreign_key: true
  end
end
