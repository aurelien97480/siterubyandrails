class AdminController < ApplicationController
    before_action :my_Init, only: [
        :index, 
        :contact, 
        :post, 
        :galery, 
        :contact_send, 
        :contact_delete, 
        :post_new, 
        :post_edit, 
        :galery_delete,
        :galery_new,
    ]
    
    def index
        @menu_selected = "Home"
    end

    def login
       @IsAdmin = 0
       if session[:auth_user] != nil
          session.delete(:auth_user)
          redirect_to '/Admin/login', flash: { Message: "Vous être deconnecter !" }
       end
    end

    def Postlogin
       @IsAdmin = 0
       user = User.where(params.require(:user).permit(:email))
 
       if user.count == 1
            if user[0].password == params[:user][:password]
                 session[:auth_user] =  {
                    id: user[0].id,
                    level: user[0].level,
                 }
                 redirect_to '/Admin'
            else
                redirect_to '/Admin/login', flash: { Message: "Information invalide !" }
            end
       else
            redirect_to '/Admin/login', flash: { Message: "Information invalide !" }
       end

    end

    def contact
       @menu_selected = "Contact"
       @contact =  Contact.all
    end

    def contact_send
        
       @contact = Contact.find(params[:id])
       @id = params[:id]

       #@contact.email

    end

    def post_contact_send
        
        redirect_to '/admin/contact'
    end

    def contact_delete
        @contact = Contact.find(params[:id])
        @contact.destroy
        redirect_to '/admin/contact'
    end

    def post
        @menu_selected = "Post"
        @post = Post.all
    end

    def post_new
        @post = Post.new

        if request.post?
            #upload :thumbnail

            thumbnail_url  ="upload/"
           
            if params[:post][:thumbnail] != nil
                @post.thumbnail = params[:post][:thumbnail].original_filename
                
                uid = SecureRandom.uuid
                ext = File.extname(params[:post][:thumbnail].original_filename)
                thumbnail_url = thumbnail_url + uid + ext
                print "\n\n======\n"+thumbnail_url+"\n======\n\n"

                File.open(Rails.root.join('public', 'upload', uid + ext), 'wb') do |file|
                    file.write(params[:post][:thumbnail].read)
                end

                params[:post][:thumbnail] = thumbnail_url
            end
            
            @post = Post.new(params.require(:post).permit(:title, :content, :thumbnail))
            if @post.save
                redirect_to '/Admin/post'
            else
                render 'post_new'
            end
        end
    end

    def post_edit
        @post = Post.find(params[:id])

        if request.post?
            #upload :thumbnail

            thumbnail_url = "upload/"

            #params[:post][:thumbnail] is string ?
            
            unless params[:post][:thumbnail].instance_of?(String)
                if params[:post][:thumbnail] != nil
                    @post.thumbnail = params[:post][:thumbnail].original_filename
                    
                    uid = SecureRandom.uuid
                    ext = File.extname(params[:post][:thumbnail].original_filename)
                    thumbnail_url = thumbnail_url + uid + ext
                    print "\n\n======\n"+thumbnail_url+"\n======\n\n"

                    File.open(Rails.root.join('public', 'upload', uid + ext), 'wb') do |file|
                        file.write(params[:post][:thumbnail].read)
                    end

                    params[:post][:thumbnail] = thumbnail_url
                end
            end
            
            @post = Post.find(params[:id])
            if @post.update(params.require(:post).permit(:title, :content, :thumbnail))
                redirect_to '/Admin/post'
            else
                render 'post_edit'
            end
        end
         
    end

    def post_delete
        @post = Post.find(params[:id])
        @post.destroy
        redirect_to '/Admin/post'
    end

    

    def galery
        @menu_selected = "Galery"
        @galery =  Galery.all
    end

    def galery_new
        if request.post?
            
            #url_base
            
            image_url = root_url + "upload/"

            unless params[:galery][:image].instance_of?(String)
                if params[:galery][:image] != nil
                    
                    
                    uid = SecureRandom.uuid
                    ext = File.extname(params[:galery][:image].original_filename)
                    image_url = image_url + uid + ext

                    print "\n\n======\n\n"+image_url+"\n======\n\n"

                    File.open(Rails.root.join('public', 'upload', uid + ext), 'wb') do |file|
                        file.write(params[:galery][:image].read)
                    end

                    params[:galery][:url] = image_url
                end
            end
            
            #params[:galery][:category_id] =  Category.find(params[:galery][:category_id])

            print "\n\n======\n\n"
            print params[:galery][:category_id]
            print "\n\n======\n"

            @galery = Galery.new(params.require(:galery).permit(:alt, :url, :category_id))
             
            if @galery.save
                redirect_to '/Admin/galery'
            else
                render 'galery_new'
            end
        end
    end

    def galery_delete
        @galery = Galery.find(params[:id])
        @galery.destroy
        redirect_to '/Admin/galery'
    end

    private
    
    def my_Init
        @IsAdmin = 1
        @menusAdmin = [ "Home", "Galery" , "Contact", "Post" ]
        if session[:auth_user] == nil
          redirect_to '/Admin/login'
        end

         if session[:auth_user]["level"] > 0
          redirect_to '/Success' , flash: { Message: "Vous avais pas le niveau require !" }
        end
    end
end