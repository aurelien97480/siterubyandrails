class CreateGalery < ActiveRecord::Migration[7.0]
  def change
    create_table :galeries do |t|
      t.string :alt
      t.string :url
      t.string :category

      t.timestamps
    end
  end
end
