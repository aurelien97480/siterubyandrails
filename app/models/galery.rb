class Galery < ApplicationRecord
    belongs_to :categorys, class_name: 'Category', foreign_key: 'category_id'

    validates :alt, presence: true
    validates :url, presence: true
    validates :category_id, presence: true
end