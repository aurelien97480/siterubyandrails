class PagesController < ApplicationController
   before_action :my_Init

  def index
        @menu_selected = "Home"
        @posts = Post.all
  end

  def galery
      @title = "Galery"
      @menu_selected = "Galery"

      @categorys = Category.all
      
      @category = params[:category]

      @images = []
      @images = Galery.all if @category == nil
      @category = Category.where(:name => @category).first if @category != nil
      
      @images = @category.galerys if @category != nil
       
      @block1 = []
      @block2 = []
      @block3 = []

      index = 0

      print "=== categorys.count === " +  @images.count.inspect

      @images.each do |image|

            unless index <= 2
               index = 0
            end

            @block1 << image if index == 0
            @block2 << image if index == 1
            @block3 << image if index == 2

            if index <= 2
               index += 1
            end
            
      end
            
  end

  def contact
        @title = "Contact"
        @menu_selected = "Contact"
  end

  def success
        @title = "Success"
        @menu_selected = "=="
        @Message = flash[:Message]
  end

  def createcontact
     @contact = Contact.new
     if @contact.update(params.require(:contact).permit(:name, :email, :message))
        @contact.save
        redirect_to '/Success', flash: { Message: "Votre message à était envoyer !" }
     else
        redirect_to '/Success', flash: { Message: "Votre message à pas était envoyer !" }
     end
  end

   def my_Init
        @IsAdmin = 0
        @menus = [ "Home", "Galery" , "Contact" ]
   end
   
end