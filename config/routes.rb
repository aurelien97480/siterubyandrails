Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "posts#index"

  root "pages#index"

  get '/Home', to: 'pages#index'
  get '/Galery', to: 'pages#galery'
  get '/Galery/:category', to: 'pages#galery'
  
  get '/Contact', to: 'pages#contact'
  get '/Success', to: 'pages#success'

  post '/Contact', to: 'pages#createcontact'
  
  #admin

  get '/Admin', to: 'admin#index'
  get '/Admin/login', to: 'admin#login'

  post '/Admin/login', to: 'admin#Postlogin'

  get '/Admin/contact', to: 'admin#contact'
  get '/Admin/contact/send/:id', to: 'admin#contact_send'
  post '/Admin/contact/send/:id', to: 'admin#post_contact_send'
  get '/Admin/contact/delete/:id', to: 'admin#contact_delete'

  get '/Admin/post', to: 'admin#post'
  get '/Admin/post/new', to: 'admin#post_new'
  post '/Admin/post/new', to: 'admin#post_new'

  get '/Admin/post/edit/:id', to: 'admin#post_edit'
  post '/Admin/post/edit/:id', to: 'admin#post_edit'
  
  get '/Admin/post/delete/:id', to: 'admin#post_delete'

  get '/Admin/galery', to: 'admin#galery'

  get '/Admin/galery/new', to: 'admin#galery_new'
  post '/Admin/galery/new', to: 'admin#galery_new'


  get '/Admin/galery/edit/:id', to: 'admin#galery_edit'
  get '/Admin/galery/delete/:id', to: 'admin#galery_delete'

end
